# Render To Texture #

This render to texture (rentotx) python script and library drives the Arnold renderer to 
render any part of a polygon mesh's shader network to a texture image file. 
The primary purpose of the script is to provide a means to compute and store 
computationally expensive shader networks and sub-networks to fixed resolution 
texture maps for reuse. This gives the user the ability to save on computation
cost at the risk of introducing a resolution dependency, which means scaling 
up past the set resolution will result in a loss of apparent visual quality. 

## Requirements ##

1. Python 2.6.0 or greater
2. Arnold 5 
3. MtoA (the version that matches your Arnold 5 version)

## Background ##

MtoA comes with a "Render Selection to Texture" tool:

https://support.solidangle.com/display/AFMUG/Render+Selection+to+Texture

This tool came about as a Solid Angle request ticket:

Render to Texture - https://trac.solidangle.com/mtoa/ticket/2016

It requested that Solid Angle integrate Jules Stevenson's Kettle Bake shader
functionality inside MtoA. Stevenson's Kettle Bake shader can be found here:

https://bitbucket.org/Kettle/kettle_uber/wiki/Kettle_Bake

Stevenson's Kettle Bake shader is part of Kettle Studio's in-house open-source
shaders package that can be found here:

https://bitbucket.org/Kettle/kettle_uber/wiki/Home

Stevenson's Kettle Bake shader was integrated into the MtoA shaders by Solid 
Angle here:

https://trac.solidangle.com/mtoa/browser/shaders/src/CameraUvMapper.cpp

A Maya command in MtoA for the MtoaCameraUvMapper shader was created by Solid Angle 
here:

https://trac.solidangle.com/mtoa/browser/plugins/mtoa/commands/ArnoldRenderToTextureCmd.cpp

## Problem Statement ##

We found through profiling that a number of the 2D procedural nodes, like
alFlowNoise, were eating up a lot of our rendering resources. 

Unfortunately, Solid Angle's "Render Selection to Texture" tool only works in 
MtoA. A request to port it to HtoA has been around since 2016:

Bake to texture - https://trac.solidangle.com/htoa/ticket/771

Bake to texture - https://trac.solidangle.com/htoa/ticket/853

So, I had a go at porting the "Render Selection to Texture" tool to HtoA to 
address this issue. I immediately found that integrating such a tool into HtoA 
was going to take some very nasty monkey patching. Therefore, I decided to 
pivot to a more utilitarian approach. I thought that having one tool that 
could do the Arnold render to texture independent of any DCC software package 
would be more beneficial to all of the Arnold community.

## Goal ##

The goal here is to create a standalone render to texture (rentotx) script that
reads in an exported Arnold source scene (.ass) file and renders to texture any
user selected shader in the shader network (a.k.a. tree) of a user selected
polymesh node. It does this in a loop until the user quits. This approach
extends the functionality of Solid Angle's Render to Texture tool by allowing
any shader to be chosen, not just the root surface shader. This is important
to artists when they want to render to texture a shader without illumination.

## Limitations ##

* Only works with polygon meshes (polymesh nodes), no support for subdivision
  surfaces
* Only works with polygon meshes (polymesh nodes) with a well defined UV set -
  unique UV's are required for the geometry that you wish to render to a 
  texture because Arnold does not allow different sets of UV's, any textures 
  that are tiled will not work as expected 
* Normals for the geometry must be pointing in the correct direction prior to 
  texture baking

## Environment ##

If you are not a MtoA user, download and install the MtoA version that matches 
your Arnold 5 version. MtoA can be downloaded from:

https://downloads.solidangle.com/mtoa/

The rentotx script will respond to two environment variables:

1. ARNOLD_PLUGIN_PATH - The environment variable that is used to automatically
   find and load plugins (e.g. Arnold shaders, procedurals, volumes, drivers,
   and etc.). This is the same environment variable used by kick and the DCC
   plugins.

2. RENTOTX_MTOA_SHADERS - The rentotx script uses MtoA's MtoaCameraUvMapper. This 
   environment variable provides a way for people not using MtoA to specify 
   the MtoA shader search path, without having to pollute the other environment
   variables.

## Usage ##

The tool is a standalone script called rentotx. You must first export from
your DCC software of choice to an .ass file. The script reads the .ass file and
renders to texture one or more shader networks.

The script currently had two modes: interactive and batch.

**Interactive Mode** - This mode uses an interactive command line interface. 
You can pass command line arguments to set defaults. When it runs, it will ask
you to confirm input. If you say no, it will allow you to input a different
value instead.

**Batch Mode** - This mode is for the user that already knows which polymesh 
node and shader they want to use. All input is taken from command line 
arguments.

The mode is set with a command line argument command string, similar to SVN or 
git.

~~~
> rentotx interactive ./test.ass ./
~~~

To get help for the batch mode:

~~~
> rentotx batch -h
~~~

We have adopted the <udim> token in file names in both modes, like Arnold
does:

https://support.solidangle.com/pages/viewpage.action?pageId=40110950


## Example ##

There is an example of how to run rentotx in the example directory in the 
directory containing this file. It demonstrates how to run the rentotx in both 
interactive and batch mode.

## Test ##

There is a test suite that uses python's unittest module. It can be ran with:

~~~
> python -m unittest discover -v rentotx
~~~

## Future Plans ##

* We would like to remove the dependency on MtoA by creating a MtoaCameraUvMapper 
  lens shader of our own in this project
* We will look into supporting displacement maps as well
