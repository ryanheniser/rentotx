import sys

import arnold

# Developed for and tested with Arnold 5
_versionList = arnold.AiGetVersion()
if int(_versionList[1]) > 5:
    print("Only Arnold 5 is currently supported (using %s.%s.%s)" %
          tuple(_versionList))
    sys.exit(1)

# Minimum requirement for Arnold Python bindings is Python 2.6.0,  due to use
# of the 'ctypes' module and 'c_bool'
if sys.hexversion < 0x02060000:
    print("Arnold needs Python 2.6.0 or greater (found Python %d.%d.%d)" %
          (sys.version_info[0], sys.version_info[1], sys.version_info[2]))
    sys.exit(1)

from .udim import MariUDimlTile
from .facade import RenderToTextureFacade
from .mediator import (
    UserInterfaceAndArnoldMediator, ArnoldColleague,
    InteractiveCommandLineColleague, BatchCommandLineColleague, PyQtColleague
)
from .message import info, warn, error
from .exception import BaseError, MissingRequirmentError, UserRequestedExit



