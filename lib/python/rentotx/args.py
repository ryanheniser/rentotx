import os.path
import argparse

from .path import normAbsRealPath

class ReadableFileAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        prospective_file = values

        if not os.path.isfile(prospective_file):
            raise argparse.ArgumentTypeError('%s is not a valid file' % (
                prospective_file,
            ))

        if os.access(prospective_file, os.R_OK):
            setattr(namespace, self.dest, normAbsRealPath(prospective_file))
            return

        raise argparse.ArgumentTypeError('%s is not a readable directory' % (
            prospective_file,
        ))

def addRequiredAssFileArg(parser):
    parser.add_argument("ass_file_path",
                        help="arnold scene source (.ass) file path",
                        type=str,
                        action=ReadableFileAction)

def addRequiredPolymeshAndShaderNodeNamesArgs(parser):
    parser.add_argument("polymesh_node_name",
                        help="the name of the polymesh node",
                        type=str)
    parser.add_argument("shader_node_name",
                        help="the name of the shader node in the polymesh "
                             "node's shader network",
                        type=str)

def addOptoinalCommonArgs(parser):
    parser.add_argument("--near_clip",
                        help="the near clipping plane of the camera's "
                             "renderable area",
                        type=float,
                        default=0.00010)
    parser.add_argument("--offset",
                        help="the distance to offset the camera position from "
                             "the surface point along the normal (used to "
                             "avoid missing the surface due to floating point "
                             "imprecision)",
                        type=float,
                        default=0.001)
    parser.add_argument("--u_offset",
                        help="where the output image will start/end in the U "
                             "range",
                        type=float,
                        default=0.0)
    parser.add_argument("--v_offset",
                        help="where the output image will start/end in the V "
                             "range",
                        type=float,
                        default=0.0)
    parser.add_argument("--uv_set",
                        help="which UV set to use",
                        type=str,
                        default="")
    parser.add_argument("--u_scale",
                        help="how the output image will scale in the U range",
                        type=float,
                        default=1.0)
    parser.add_argument("--v_scale",
                        help="how the output image will scale in the V range",
                        type=float,
                        default=1.0)


def addOptionalEnableUdimArg(parser):
    parser.add_argument("--enable_udim",
                        help="enable the computing and usage of all UDim "
                             "values",
                        action='store_true')
