import arnold as ar

# ============================================================
# Parameter / User Parameter Values to String
# ============================================================

def _boolToStr(b):
    return "true" if (b != 0) else "false"

def _rgbToStr(c):
    return "(%0.5f, %0.5f, %0.5f)" % (c.r, c.g, c.b)

def _rgbaToStr(c):
    return "(%0.5f, %0.5f, %0.5f, %0.5f)" % (c.r, c.g, c.b, c.a)

def _vecToStr(v):
    return "(%0.5f, %0.5f, %0.5f)" % (v.x, v.y, v.z)

def _pntToStr(p):
    return "(%0.5f, %0.5f, %0.5f)" % (p.x, p.y, p.z)

def _pnt2ToStr(p):
    return "(%0.5f, %0.5f)" % (p.x, p.y)

def _nodePtrToStr(p):
    if not bool(p): return "" # NULL pointers have a False boolean value
    node = ar.cast(p, ar.POINTER(ar.AtNode))
    return ar.AiNodeGetName(node)

def _nodeParamToStr(node, paramName):
    ptr = ar.AiNodeGetPtr(node, paramName)
    return _nodePtrToStr(ptr)

def _enumToStr(pe, i):
    return ar.AiEnumGetString(ar.AiParamGetEnum(pe), i)

def _enumParamToStr(node, paramName):
    nodeEntry = ar.AiNodeGetNodeEntry(node)
    paramEntry = ar.AiNodeEntryLookUpParameter(nodeEntry, paramName)
    return _enumToStr(paramEntry, ar.AiNodeGetInt(node, paramName))

def _mtxToStr(m):
    attrs = ['a' + str(i) + str(j) for i in xrange(4) for j in xrange(4)]
    return str([m.__getattribute__(c) for c in attrs])

def _matrixParamToStr(node, paramName):
    m = ar.AtMatrix()
    ar.AiNodeGetMatrix(node, paramName, m)
    return _mtxToStr(m)

def _matrixArrayElemToStr(a, i):
    m = ar.AtMatrix()
    ar.AiArrayGetMtx(a, i, m)
    return _mtxToStr(m)

def _arrayToStr(a):
    array = a.contents
    nelems = array.nelements
    if nelems == 0:
        return "[]"
    result = "["
    for i in range(nelems):
        result += _arrayElemToStrFunc[array.type](array, i)
        if i < (nelems-1): result += ", "
    result += "]"
    return result

_arrayElemToStrFunc = {
    ar.AI_TYPE_BYTE     : lambda a, i: str(ar.AiArrayGetByte(a, i)),
    ar.AI_TYPE_INT      : lambda a, i: str(ar.AiArrayGetInt(a, i)),
    ar.AI_TYPE_UINT     : lambda a, i: str(ar.AiArrayGetUInt(a, i)),
    ar.AI_TYPE_BOOLEAN  : lambda a, i: _boolToStr(ar.AiArrayGetBool(a, i)),
    ar.AI_TYPE_FLOAT    : lambda a, i: "%0.5f" % ar.AiArrayGetFlt(a, i),
    ar.AI_TYPE_RGB      : lambda a, i: _rgbToStr(ar.AiArrayGetRGB(a, i)),
    ar.AI_TYPE_RGBA     : lambda a, i: _rgbaToStr(ar.AiArrayGetRGBA(a, i)),
    ar.AI_TYPE_VECTOR   : lambda a, i: _vecToStr(ar.AiArrayGetVec(a, i)),
    ar.AI_TYPE_POINT    : lambda a, i: _pntToStr(ar.AiArrayGetPnt(a, i)),
    ar.AI_TYPE_POINT2   : lambda a, i: _pnt2ToStr(ar.AiArrayGetPnt2(a, i)),
    ar.AI_TYPE_STRING   : lambda a, i: "%s" % ar.AiArrayGetStr(a, i),
    ar.AI_TYPE_POINTER  : lambda a, i: str(ar.AiArrayGetPtr(a, i)),
    ar.AI_TYPE_NODE     : lambda a, i: _nodePtrToStr(ar.AiArrayGetPtr(a, i)),
    ar.AI_TYPE_ARRAY    : lambda a, i: _arrayToStr(ar.AiArrayGetArray(a, i)),
    ar.AI_TYPE_MATRIX   : lambda a, i: _matrixArrayElemToStr(a, i)
}

_valueToStrFunc = {
    ar.AI_TYPE_BYTE     : lambda n, pn: str(ar.AiNodeGetByte(n, pn)),
    ar.AI_TYPE_INT      : lambda n, pn: str(ar.AiNodeGetInt(n, pn)),
    ar.AI_TYPE_UINT     : lambda n, pn: str(ar.AiNodeGetUInt(n, pn)),
    ar.AI_TYPE_BOOLEAN  : lambda n, pn: _boolToStr(ar.AiNodeGetBool(n, pn)),
    ar.AI_TYPE_FLOAT    : lambda n, pn: "%0.5f" % ar.AiNodeGetFlt(n, pn),
    ar.AI_TYPE_RGB      : lambda n, pn: _rgbToStr(ar.AiNodeGetRGB(n, pn)),
    ar.AI_TYPE_RGBA     : lambda n, pn: _rgbaToStr(ar.AiNodeGetRGBA(n, pn)),
    ar.AI_TYPE_VECTOR   : lambda n, pn: _vecToStr(ar.AiNodeGetVec(n, pn)),
    ar.AI_TYPE_POINT    : lambda n, pn: _pntToStr(ar.AiNodeGetPnt(n, pn)),
    ar.AI_TYPE_POINT2   : lambda n, pn: _pnt2ToStr(ar.AiNodeGetPnt2(n, pn)),
    ar.AI_TYPE_STRING   : lambda n, pn: "%s" % ar.AiNodeGetStr(n, pn),
    ar.AI_TYPE_POINTER  : lambda n, pn: str(ar.AiNodeGetPtr(n, pn)),
    ar.AI_TYPE_NODE     : lambda n, pn: _nodeParamToStr(n, pn),
    ar.AI_TYPE_ARRAY    : lambda n, pn: _arrayToStr(ar.AiNodeGetArray(n, pn)),
    ar.AI_TYPE_MATRIX   : lambda n, pn: _matrixParamToStr(n, pn),
    ar.AI_TYPE_ENUM     : lambda n, pn: _enumParamToStr(n, pn)
}

# ============================================================
# Parameter Defaults to String
# ============================================================

def _boolDefaultToStr(d, _):
    return _boolToStr(d.contents.BOOL)

def _rgbDefaultToStr(d, _):
    return _rgbToStr(d.contents.RGB)

def _rgbaDefaultToStr(d, _):
    return _rgbaToStr(d.contents.RGBA)

def _vecDefaultToStr(d, _):
    return _pntToStr(d.contents.PNT)

def _pntDefaultToStr(d, _):
    return _pntToStr(d.contents.PNT)

def _pnt2DefaultToStr(d, _):
    return _pnt2ToStr(d.contents.PNT)

def _nodeDefaultToStr(d, _):
    return _nodePtrToStr(d.contents.PTR)

def _mtxDefaultToStr(d, _):
    return _mtxToStr(d.contents.pMTX)

def _enumDefaultToStr(d, pe):
    return _enumToStr(pe, d.contents.INT)

def _arrayDefaultToStr(d, _):
    return _arrayToStr(d.contents.ARRAY)

_defaultToStrFunc = {
    ar.AI_TYPE_BYTE     : lambda d, pe: str(d.contents.BYTE),
    ar.AI_TYPE_INT      : lambda d, pe: str(d.contents.INT),
    ar.AI_TYPE_UINT     : lambda d, pe: str(d.contents.UINT),
    ar.AI_TYPE_BOOLEAN  : _boolDefaultToStr,
    ar.AI_TYPE_FLOAT    : lambda d, pe: "%0.5f" % d.contents.FLT,
    ar.AI_TYPE_RGB      : _rgbDefaultToStr,
    ar.AI_TYPE_RGBA     : _rgbaDefaultToStr,
    ar.AI_TYPE_VECTOR   : _pntDefaultToStr,
    ar.AI_TYPE_POINT    : _pntDefaultToStr,
    ar.AI_TYPE_POINT2   : _pnt2DefaultToStr,
    ar.AI_TYPE_STRING   : lambda d, pe: "%s" % d.contents.STR,
    ar.AI_TYPE_POINTER  : lambda d, pe: str(d.contents.PTR),
    ar.AI_TYPE_NODE     : _nodeDefaultToStr,
    ar.AI_TYPE_ARRAY    : _arrayDefaultToStr,
    ar.AI_TYPE_MATRIX   : _mtxDefaultToStr,
    ar.AI_TYPE_ENUM     : _enumDefaultToStr,
}

def _getParams(node, nodeEntry, nParams, trunc=None):
    params = []
    for i in range(nParams):
        paramEntry = ar.AiNodeEntryGetParameter(nodeEntry, i)
        paramType = ar.AiParamGetType(paramEntry)
        default = ar.AiParamGetDefault(paramEntry)
        defaultStr = _defaultToStrFunc[paramType](default, paramEntry)

        if paramType == ar.AI_TYPE_ARRAY:
            array = default.contents.ARRAY.contents
            paramTypeStr = "%s[]" % ar.AiParamGetTypeName(array.type)
        else:
            paramTypeStr = ar.AiParamGetTypeName(paramType)

        paramName = ar.AiParamGetName(paramEntry)

        valueStr = _valueToStrFunc[paramType](node, paramName)

        if trunc:
            if len(valueStr) > trunc:
                valueStr = valueStr[:trunc] + "..."
            if len(defaultStr) > trunc:
                defaultStr = defaultStr[:trunc] + "..."

        params.append(
            {
                "index"     : i,
                "type"      : paramTypeStr,
                "name"      : paramName,
                "default"   : defaultStr,
                "value"     : valueStr
            }
        )

    return params

_categoryToStr = {
    ar.AI_USERDEF_CONSTANT :  "CONSTANT",
    ar.AI_USERDEF_UNIFORM  :  "UNIFORM",
    ar.AI_USERDEF_VARYING  :  "VARYING",
    ar.AI_USERDEF_INDEXED  :  "INDEXED"
}

def _getUserParams(node, trunc=None):
    params = []
    userParamIter = ar.AiNodeGetUserParamIterator(node)
    while not ar.AiUserParamIteratorFinished(userParamIter):
        userParamEntry = ar.AiUserParamIteratorGetNext(userParamIter)
        categoryStr = _categoryToStr[ar.AiUserParamGetCategory(userParamEntry)]

        userParamType = ar.AiUserParamGetType(userParamEntry)
        if userParamType == ar.AI_TYPE_ARRAY:
            arrayType = ar.AiUserParamGetArrayType(userParamEntry)
            userParamTypeStr = "%s[]" % ar.AiParamGetTypeName(arrayType)
        else:
            userParamTypeStr = ar.AiParamGetTypeName(userParamType)

        userParamName = ar.AiUserParamGetName(userParamEntry)

        valueStr = _valueToStrFunc[userParamType](node, userParamName)

        if trunc and len(valueStr) > trunc:
            valueStr = valueStr[:trunc] + "..."

        params.append(
            {
                "index"     : ar.AiUserParamGetIndex(userParamEntry),
                "category"  : categoryStr,
                "type"      : userParamTypeStr,
                "name"      : userParamName,
                "value"     : valueStr
            }
        )

    ar.AiUserParamIteratorDestroy(userParamIter)
    return params

def getNodeInfo(node, trunc=None):
    nodeEntry = ar.AiNodeGetNodeEntry(node)
    outputType = ar.AiNodeEntryGetOutputType(nodeEntry)
    filename = ar.AiNodeEntryGetFilename(nodeEntry)
    if filename is None: filename = "<built-in>"
    num_params = ar.AiNodeEntryGetNumParams(nodeEntry)

    info = {
        "name"          : ar.AiNodeGetName(node),
        "entry_name"    : ar.AiNodeEntryGetName(nodeEntry),
        "type"          : ar.AiNodeEntryGetTypeName(nodeEntry),
        "output_type"   : ar.AiParamGetTypeName(outputType),
        "num_params"    : num_params,
        "filename"      : filename,
        "version"       : ar.AiNodeEntryGetVersion(nodeEntry),
        "params"        : _getParams(node, nodeEntry, num_params, trunc),
        "user_params"   : _getUserParams(node, trunc)
    }
    info["num_user_params"] = len(info["user_params"])

    return info

_nodeFmtStr = \
"""
name:       %(name)s
entry:      %(entry_name)s
type:       %(type)s
output:     %(output_type)s
filename:   %(filename)s
version:    %(version)s"""

_paramFmtStr = \
"""
  %(name)s
    type:       %(type)s
    default:    %(default)s
    value:      %(value)s"""

_userParamFmtStr = \
"""
  %(name)s
    category:   %(category)s
    type:       %(type)s
    value:      %(value)s"""

def printNodeInfo(node, trunc=None):
    print 50*"="

    info = getNodeInfo(node, trunc)
    print _nodeFmtStr % info

    print "\nParameters (%d):" % info["num_params"]
    for param in info["params"]:
        print  _paramFmtStr % param

    print "\nUser Parameters (%d):" % info["num_params"]
    for userParam in info["user_params"]:
        print  _userParamFmtStr % userParam

    print ""
    print 50*"="

def printNodeInfoForAllNodesOfType(mask=ar.AI_NODE_ALL, trunc=None):
    nodeIter = ar.AiUniverseGetNodeIterator(mask)
    while not ar.AiNodeIteratorFinished(nodeIter):
        node = ar.AiNodeIteratorGetNext(nodeIter)
        printNodeInfo(node, trunc)
    ar.AiNodeIteratorDestroy(nodeIter)


## Boilerplate code to do remote debugging
## ---------------------------------------------------
#test = False
#if test:
#    import sys
#    PYCHARM_EGG_PATH = "/mnt/scratch/rheniser/pycharm-5.0/debug-eggs/pycharm-debug.egg"
#    if PYCHARM_EGG_PATH not in sys.path:
#        sys.path.append(PYCHARM_EGG_PATH)
#    import pydevd
#    pydevd.settrace('localhost', port=9999, stdoutToServer=True,
#                    stderrToServer=True)
#    # ---------------------------------------------------
