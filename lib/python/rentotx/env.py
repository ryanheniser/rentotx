import os

def renToTxMtoAShadersEnvVar(default=""):
    return os.getenv("RENTOTX_MTOA_SHADERS", default)
