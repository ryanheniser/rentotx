class BaseError(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class MissingRequirmentError(BaseError):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class InvalidInputError(BaseError):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class UserRequestedExit(BaseError):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class InviablePolymeshNode (BaseError):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)
