import copy
import ctypes

import arnold as ar

from .env import renToTxMtoAShadersEnvVar
from .exception import (BaseError, MissingRequirmentError, UserRequestedExit,
                        InviablePolymeshNode)
from .message import warn, error
from .node import (makeUniqueName, getAllNodes, nodeSetArrayString,
                   confirmViablePolymeshNode )
from .tree import ShaderTree
from .udim import MariUDimlTile, getUDimValuesToUDimTile

##############################################################################
# This class provides a context manager for the Arnold scene edits for each
# separate render. It makes sure the Arnold scene is restored back to its
# original state, as in before any scene edits were made for a render. This
# makes it eaiser to create multiple different renders from one Arnold session.
# This works well so far as there are only a few edits; But, it should not be
# used for a lot of edits. Capturing the scene as an ass file, clearing the
# scene, and restoring the scene with the ass file would be a better choice
# when there are a lot of edits (i.e. memento pattern).
class RenderSceneEdits(object):

    def __init__(self, polymeshNode):

        # Capture the parts of the scene we will need to both perform the edits
        # and restore the scene to its original state upon exit
        self._polymeshNode = polymeshNode
        self._polymeshNodeName = ar.AiNodeGetName(polymeshNode)

        self._optionsNode = ar.AiUniverseGetOptions()

        self._outputs = self._getOutputsArrayParamOnOptionsNode()

        self._originalOptionsCameraNodePtr = \
            ar.AiNodeGetPtr(self._optionsNode, "camera")

        self._originalOutputsCameraNode = self._getOutputsPrimaryCameraNode()

        self._originalOutputsCameraNodeName = \
            ar.AiNodeGetName(self._originalOutputsCameraNode)

        self._originalDriverNodePtrsToFilenames = \
            self._getOriginalDriverNodePtrsToFilenamesForOriginalCamera()

        self._originalShapeNodePtrsToVisibility = \
            self._getOriginalShapeNodePtrsToVisbility()

        self._polymeshNodeSidedness = ar.AiNodeGetByte(self._polymeshNode,
                                                       "sidedness")

        self._cameraUvMapperNode = None
        self._cameraUvMapperNodeName = None

    def _getOutputsArrayParamOnOptionsNode(self):
        # Get the outputs string array param as a python list
        array = ar.AiNodeGetArray(self._optionsNode, 'outputs').contents
        nelements = ar.AiArrayGetNumElements(array)
        assert nelements >= 1, "there should always be at least one output"
        return [ar.AiArrayGetStr(array, i) for i in xrange(nelements)]

    def _getOutputsPrimaryCameraNode(self):
        # Try the new style of specifying the primary camera, which is the
        # outputs array on the options node first
        name = self._outputs[0].split(' ')[0]
        cameraNode = ar.AiNodeLookUpByName(name)
        if cameraNode: return cameraNode

        # Try the old style of specifying the primary camera, which is the
        # camera parameter on the options node.
        cameraNodePtr = ar.AiNodeGetPtr(self._optionsNode, "camera")
        if cameraNodePtr is None: raise RuntimeError("no camera specified")
        return ar.AtNode.from_address(cameraNodePtr)

    def _getOriginalDriverNodePtrsToFilenamesForOriginalCamera(self):
        result = {}
        driverNodes = getAllNodes(ar.AI_NODE_DRIVER)
        for i in range(len(self._outputs)):
            for token in self._outputs[i].split(' '):
                for driverNode in driverNodes:
                    if token == ar.AiNodeGetName(driverNode):
                        fn = ar.AiNodeGetStr(driverNode, "filename")
                        result[ctypes.addressof(driverNode.contents)] = fn
        return result

    @staticmethod
    def _getOriginalShapeNodePtrsToVisbility():
        result = {}
        nodeIter = ar.AiUniverseGetNodeIterator(ar.AI_NODE_SHAPE)
        while not ar.AiNodeIteratorFinished(nodeIter):
            shapeNode = ar.AiNodeIteratorGetNext(nodeIter)
            result[ctypes.addressof(shapeNode.contents)] = \
                ar.AiNodeGetByte(shapeNode, "visibility")
        ar.AiNodeIteratorDestroy(nodeIter)
        return result

    # Enter the runtime context and return this object to the runtime context.
    # The value returned by this method is bound to the identifier in the as
    # clause of with statements using this context manager.
    def __enter__(self):
        return self

    #######################################
    # Scene Edits / Restore Methods
    #######################################

    def _editCreateCameraUvMapperNode(self, uDimTile, nearClip, offset,
                                      uOffset, vOffset, uvSet, uScale, vScale):

        self._cameraUvMapperNodeName = makeUniqueName("cameraUvBaker")
        self._cameraUvMapperNode = ar.AiNode("MtoaCameraUvMapper",
                                             self._cameraUvMapperNodeName)
        ar.AiNodeSetStr(self._cameraUvMapperNode, "polymesh",
                        self._polymeshNodeName)
        ar.AiNodeSetFlt(self._cameraUvMapperNode, "near_clip", nearClip)
        ar.AiNodeSetFlt(self._cameraUvMapperNode, "offset", offset)
        ar.AiNodeSetFlt(self._cameraUvMapperNode, "u_offset", uOffset)
        ar.AiNodeSetFlt(self._cameraUvMapperNode, "v_offset", vOffset)
        ar.AiNodeSetStr(self._cameraUvMapperNode, "uv_set", uvSet)
        ar.AiNodeSetFlt(self._cameraUvMapperNode, "u_scale", uScale)
        ar.AiNodeSetFlt(self._cameraUvMapperNode, "v_scale", vScale)
        if uDimTile:
            ar.AiNodeSetFlt(self._cameraUvMapperNode, "u_offset",
                            -float(uDimTile.intU))
            ar.AiNodeSetFlt(self._cameraUvMapperNode, "v_offset",
                            -float(uDimTile.intV))

    def _restoreDestroyCameraUvMapperNode(self):
        ar.AiNodeDestroy(self._cameraUvMapperNode)

    def _editReplaceUdimTokenInFilenameParamsOnDriverNodes(self, uDimValue):
        if uDimValue is None: return
        # Replace <udim> tokens in the filename string parameters on the
        # driver nodes when there is a uDimValue
        for ptr, filename in self._originalDriverNodePtrsToFilenames.items():
            nfn = filename.replace('<udim>', str(uDimValue))
            driverNode = ar.AtNode.from_address(ptr)
            ar.AiNodeSetStr(driverNode, "filename", nfn)

    def _restoreFileNameParamsOnDriverNodes(self):
        for ptr, filename in self._originalDriverNodePtrsToFilenames.items():
            driverNode = ar.AtNode.from_address(ptr)
            ar.AiNodeSetStr(driverNode, "filename", filename)

    def _editReplaceOrigCamWithCamUvMapperInOutputsParamOnOptions(self):
        newOutputs = []
        for i in range(len(self._outputs)):
            newOutputs.append(
                self._outputs[i].replace(self._originalOutputsCameraNodeName,
                                         self._cameraUvMapperNodeName))
        nodeSetArrayString(self._optionsNode, 'outputs', newOutputs)

    def _restoreOutputsParamOnOptionsNode(self):
        nodeSetArrayString(self._optionsNode, 'outputs', self._outputs)


    def _editSetCameraParamOnOptionsNode(self):
        ar.AiNodeSetPtr(self._optionsNode, "camera", self._cameraUvMapperNode)

    def _restoreCameraParamOnOptionsNode(self):
        ar.AiNodeSetPtr(self._optionsNode, "camera",
                        self._originalOptionsCameraNodePtr)

    def _editSetVisibilityParamOnShapeNodes(self):

        # Set all shape nodes to be invisible to camera rays
        mask = ( ar.AI_RAY_SHADOW|ar.AI_RAY_DIFFUSE_TRANSMIT|
                 ar.AI_RAY_SPECULAR_TRANSMIT|ar.AI_RAY_DIFFUSE_REFLECT|
                 ar.AI_RAY_SPECULAR_REFLECT )
        for ptr, vis in self._originalShapeNodePtrsToVisibility.items():
            shapeNode = ar.AtNode.from_address(ptr)
            name = ar.AiNodeGetName(shapeNode)
            if name == "root":  # Avoid setting the shape node "root" invisible
                continue         # as it would make the whole scene invisible
            ar.AiNodeSetByte(shapeNode, "visibility", mask)

        ar.AiNodeSetByte(self._polymeshNode, "visibility", ar.AI_RAY_ALL)

    def _restoreVisibilityParamOnShapeNodes(self):
        for ptr, vis in self._originalShapeNodePtrsToVisibility.items():
            shapeNode = ar.AtNode.from_address(ptr)
            ar.AiNodeSetByte(shapeNode, "visibility", vis)

    def _editSetSidednessParamOnPolymeshNode(self):
        ar.AiNodeSetByte(self._polymeshNode, "sidedness", ar.AI_RAY_UNDEFINED)

    def _restoreSidednessParamOnPolymeshNode(self):
        ar.AiNodeSetByte(self._polymeshNode, "sidedness",
                         self._polymeshNodeSidedness)

    #######################################
    # Execute Edits
    #######################################

    def execute(self, uDimValue, uDimTile, nearClip,
                offset, uOffset, vOffset, uvSet, uScale, vScale):

        self._editCreateCameraUvMapperNode(uDimTile, nearClip, offset, uOffset,
                                           vOffset, uvSet, uScale, vScale)
        self._editReplaceUdimTokenInFilenameParamsOnDriverNodes(uDimValue)
        self._editReplaceOrigCamWithCamUvMapperInOutputsParamOnOptions()
        self._editSetCameraParamOnOptionsNode()
        self._editSetVisibilityParamOnShapeNodes()
        self._editSetSidednessParamOnPolymeshNode()

    # Exit runtime context and return a bool flag indicating if any exception
    # that occurred should be suppressed
    def __exit__(self, exceptionType, exceptionValue, _):
        # The exception passed in should never be reraised explicitly;
        # Instead, return a false value to indicate that the method completed
        # successfully and does not want to suppress the raised exception.
        # This allows context management code to easily detect whether or not
        # an __exit__() method has actually failed.

        # If the context was exited without an exception, all three arguments
        # will be None.
        if exceptionType is None:
            # Restore the scene When there is no exception
            self._restoreSidednessParamOnPolymeshNode()
            self._restoreVisibilityParamOnShapeNodes()
            self._restoreCameraParamOnOptionsNode()
            self._restoreOutputsParamOnOptionsNode()
            self._restoreFileNameParamsOnDriverNodes()
            self._restoreDestroyCameraUvMapperNode()

            # Return true to continue execution immediately following the with
            # statement.
            return True

        # False means exception continues propagating after this method has
        # finished executing.
        return False

##############################################################################
# Provides a simpler and unified interface to the parts of the Arnold interface
# needed to perform a render to textures for a shader node. It defines a
# higher-level interface that makes the subsystem easier and more efficient
# to use by clients for the purpose of rendering shader nodes to textures.
# The class should gracefully handle any differences between the APIs of
# Arnold versions.
#
# This class also provides a context manager. Python provides special syntax
# for context mangers in the "with" statement, which automatically manages
# resources encapsulated within context manager types, In the case here, it
# starts an Arnold process with the AiBegin and stops an Arnold process with
# AiEnd around a block of code, handling any exceptions accordingly.
class RenderToTextureFacade(object):

    def __init__(self, logFilePath=None, uDimTileClass=MariUDimlTile):
        # Log file path (a value of None will go log to the console)
        self._logFilePath = logFilePath
        # A flat shader the will be crated to be used in the loop
        self._flatShaderNode = None
        # A class to define UDims, allowing it to be overriden by the user
        self._uDimTileClass = uDimTileClass
        # A dictionary cache of polymesh node names keys to their Arnold
        # nodes value that have all the properties that allow for render to
        # texture to be performed
        self._viablePolymeshNodeNamesToNodesCache = None
        self._polymeshNodeDataCache = {}
        self._returnCode = ar.AI_ERROR

    ###########################################
    # Context Manger Methods
    ###########################################

    # Enter the runtime context and return this object to the runtime context.
    # The value returned by this method is bound to the identifier in the as
    # clause of with statements using this context manager.
    def __enter__(self):
        ar.AiBegin() # Begin Arnold process
        if self._logFilePath is None:
            ar.AiMsgSetConsoleFlags(ar.AI_LOG_ALL)
        else:
            ar.AiMsgSetLogFileName(self._logFilePath)
            ar.AiMsgSetLogFileFlags(ar.AI_LOG_ALL)
        return self

    # Exit runtime context and return a bool flag indicating if any exception
    # that occurred should be suppressed
    def __exit__(self, exceptionType, exceptionValue, _):
        # The exception passed in should never be reraised explicitly;
        # Instead, return a false value to indicate that the method completed
        # successfully and does not want to suppress the raised exception.
        # This allows context management code to easily detect whether or not
        # an __exit__() method has actually failed.

        # False means exception continues propagating after this method has
        # finished executing.
        returnValue = False
        if exceptionType is None:
            # If the context was exited without an exception, all three
            # arguments will be None. Return true to continue execution
            # immediately following the with statement.
            returnValue = True
        elif issubclass(exceptionType, BaseError):
            if exceptionType is UserRequestedExit:
                pass
            else:
                error(str(exceptionValue))
            # Returning a true value will cause the with statement to suppress
            # any exception and continue execution with the statement
            # immediately following the with statement.
            returnValue = True
        ar.AiEnd() # End Arnold process (regardless of how the exit came to be)
        return returnValue

    ###########################################
    # Cache Methods
    ###########################################

    @property
    def _viablePolymeshNodeNamesToNodes(self):
        if self._viablePolymeshNodeNamesToNodesCache is None:
            self._viablePolymeshNodeNamesToNodesCache = {}
            nodeIter = ar.AiUniverseGetNodeIterator(ar.AI_NODE_SHAPE)
            while not ar.AiNodeIteratorFinished(nodeIter):
                node = ar.AiNodeIteratorGetNext(nodeIter)
                # Filter out all node except polymesh nodes
                if not ar.AiNodeIs(node, "polymesh"): continue
                # Confirm polymesh node is viable for render to texture
                name = ar.AiNodeGetName(node)
                try:
                    confirmViablePolymeshNode(node)
                except InviablePolymeshNode, e:
                    warn("Ignoring polymesh node %s - %s" % (name, str(e)) )
                    continue
                self._viablePolymeshNodeNamesToNodesCache[name] = node
            ar.AiNodeIteratorDestroy(nodeIter)
        return self._viablePolymeshNodeNamesToNodesCache

    class PolymeshNodeDataCache(object):

        def __init__(self, polymeshNode, uDimTileClass):
            self.node = polymeshNode # Polymesh node
            self.nodeName = ar.AiNodeGetName(polymeshNode) # Polymesh node name
            self.uDimValuesToUDimTile = getUDimValuesToUDimTile(polymeshNode,
                                                                uDimTileClass)
            self.shaderTree = ShaderTree(self.node)

    def _getPolymeshNodeDataCache(self, polymeshNodeName):
        try:
            return self._polymeshNodeDataCache[polymeshNodeName]
        except KeyError:
            polymeshNode = \
                self._viablePolymeshNodeNamesToNodesCache[polymeshNodeName]
            polymeshNodeDataCache = self.PolymeshNodeDataCache(
                polymeshNode, self._uDimTileClass)
            self._polymeshNodeDataCache[polymeshNodeName] = \
                polymeshNodeDataCache
            return polymeshNodeDataCache

    ###########################################
    # Render Methods
    ###########################################

    def _renderSelectedShaderNodeToTexture(self, polymeshNodeName,
                                           shaderNodeName):
        polymeshNodeDataCache = \
            self._getPolymeshNodeDataCache(polymeshNodeName)
        shaderTreeCache = polymeshNodeDataCache.shaderTree
        parentNode, paramName = \
            shaderTreeCache.getParentNodeAndParameter(shaderNodeName)
        # Render the polymesh's surface shader to texture (with illuminance)
        if ar.AiNodeIs(parentNode, "polymesh"):
            self._returnCode = ar.AiRender()
            return

        ## Edit Scene ##
        # Set the polymesh node's shader param to a flat shader to render the
        # non-surface shader (with illuminance)
        ar.AiNodeSetPtr(polymeshNodeDataCache.node, "shader",
                            self._flatShaderNode)
        # Link flat shader to selected shader (using Arnold's data type
        # conversion to color)
        shaderNode = shaderTreeCache.getShaderNode(shaderNodeName)
        ar.AiNodeLink(shaderNode, "color", self._flatShaderNode)
        # Render a shader in the polymesh node's shader tree to texture
        # (without illuminance)
        self._returnCode = ar.AiRender()

        ## Restore Scene ##
        # Set the polymesh node's shader param back to the root shader node
        rootShaderNodeName = shaderTreeCache.getRootShaderNodeName()
        ar.AiNodeSetPtr(polymeshNodeDataCache.node, "shader",
                        rootShaderNodeName)

        ar.AiNodeUnlink(self._flatShaderNode, "color") # Unlink to clean up

        # Link the selected shader back up to its parent
        ar.AiNodeLink(shaderNode, paramName, parentNode)

    ###########################################
    # Scene Load Methods
    ###########################################

    @staticmethod
    def _confirmRequiredPluginsLoaded():
        for nodeEntryName in ["MtoaCameraUvMapper", "flat"]:
            if ar.AiNodeEntryLookUp(nodeEntryName) is None:
                raise MissingRequirmentError("Required node type %s is "
                                             "missing." % nodeEntryName)

    ###########################################
    # Scene Setup Methods
    ###########################################

    @staticmethod
    def _setOptionsNodeParameters():
        # These edits will be shared by any subsequent render

        # Ignore bump and displacements
        optionsNode = ar.AiUniverseGetOptions()
        ar.AiNodeSetBool(optionsNode, "ignore_bump", True)
        ar.AiNodeSetBool(optionsNode, "ignore_displacement", True)

        # Preserve scene data - necessary for MtoaCameraUvMapper to work
        ar.AiNodeSetBool(optionsNode, "preserve_scene_data", True)

    ###########################################
    # Public Interface
    ###########################################

    def sceneLoadAndSetup(self, sceneFilePath):
        # Load MtoA shaders from our special env variable
        ar.AiLoadPlugins(renToTxMtoAShadersEnvVar())

        ar.AiASSLoad(sceneFilePath, ar.AI_NODE_ALL)

        self._confirmRequiredPluginsLoaded()

        self._setOptionsNodeParameters()

        # Initialize polymeshes triangles (necessary for MtoaCameraUvMapper to
        # work correctly)
        self._returnCode = ar.AiRender(ar.AI_RENDER_MODE_FREE)
        ar.AiRenderAbort()

        self._flatShaderNode = ar.AiNode("flat") # Create and reuse flat shader

    def getPolymeshNodeNames(self):
        return self._viablePolymeshNodeNamesToNodes.keys()

    def getShaderNodeNames(self, polymeshNodeName):
        shaderTreeCache = \
            self._getPolymeshNodeDataCache(polymeshNodeName).shaderTree
        # Prevent cache corruption of mutable type by caller by using copy
        return copy.copy(shaderTreeCache.getShaderNodeNames())

    def getShaderTree(self, polymeshNodeName):
        shaderTreeCache = \
            self._getPolymeshNodeDataCache(polymeshNodeName).shaderTree
        # Prevent cache corruption of mutable type by caller by using copy
        return copy.deepcopy(shaderTreeCache)

    def getUDimValues(self, polymeshNodeName):
        cache = self._getPolymeshNodeDataCache(polymeshNodeName)
        return cache.uDimValuesToUDimTile.keys()

    def render(self, polymeshNodeName, shaderNodeName, uDimValue,
               nearClip, offset, uOffset, vOffset, uvSet, uScale, vScale):

        cache = self._getPolymeshNodeDataCache(polymeshNodeName)
        uDimTile = cache.uDimValuesToUDimTile[uDimValue] if uDimValue else None

        # Create a context to execute/undo scene edits for this render
        with RenderSceneEdits(cache.node) as edits:

            edits.execute(uDimValue, uDimTile, nearClip, offset, uOffset,
                          vOffset, uvSet, uScale, vScale)

            self._renderSelectedShaderNodeToTexture(polymeshNodeName,
                                                    shaderNodeName)

    @property
    def returnCode(self):
        return self._returnCode
