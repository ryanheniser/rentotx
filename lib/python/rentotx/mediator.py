import types

from .args import (
    addOptoinalCommonArgs, addRequiredPolymeshAndShaderNodeNamesArgs,
    addOptionalEnableUdimArg, addRequiredAssFileArg
)
from .exception import (InvalidInputError, UserRequestedExit,
                        MissingRequirmentError)
from .path import (isReadableFile)


# Follows the Mediator design pattern in the book:
# Design Patterns: Elements of Reusable Object-Oriented Software
# https://en.wikipedia.org/wiki/Mediator_pattern

# A class that defines an interface to an object that will encapsulate the
# communication between an user interface colleague (e.g. CLI, PyQt, PySide,
# and etc.) and an Arnold colleague to perform render to texture requests. The
# derived types will implement the mediator design pattern. With the mediator
# pattern, communication between colleague objects is encapsulated within a
# mediator object. Colleague objects do not communicate directly with each
# other, but instead communicate through the mediator. This reduces the
# dependencies between communicating colleague objects, thereby reducing
# coupling.
class UserInterfaceAndArnoldMediator(object):
    def __init__(self, userInterfaceColleague, arnoldColleague):
        super(UserInterfaceAndArnoldMediator, self).__init__()
        self._userInterfaceColleague = userInterfaceColleague
        self._arnoldColleague = arnoldColleague
        # A data channel (mutable dict) for passing data between colleagues
        self._channel = {
            "continue_loop" : True, # a sentinel value that colleagues can use
            # Scene Load
            "ass_file_path" : "",
            # Loop
            "polymesh_node_names" : [],
            "polymesh_node_name" : "",
            "shader_node_names" : [],
            "shader_node_name" : "",
            "udim_values" : [],
            "udim_value" : None, # Value of None means do not do UDim
            "near_clip" : 0.00010,
            "offset" : 0.001,
            "u_offset" : 0.0,
            "v_offset" : 0.0,
            "uv_set" : "",
            "u_scale" : 1.0,
            "v_scale" : 1.0,
            "output_file_name" : ""
        }

    def _sceneLoad(self):
        self._userInterfaceColleague.getSceneLoadData(self._channel)
        self._arnoldColleague.applySceneLoadData(self._channel)

    def _loop(self):
        while self._channel["continue_loop"]:

            self._arnoldColleague.getPolymeshNodeNames(self._channel)
            if len(self._channel["polymesh_node_names"]) == 0:
                raise MissingRequirmentError(
                    "No polymesh node (defining a polygon mesh) with a "
                    "shader and UV set was found in the scene source file "
                    "%s." % self._channel["ass_file_path"])
            self._userInterfaceColleague.selectPolymeshNodeName(self._channel)

            self._arnoldColleague.getShaderNodeNames(self._channel)
            self._userInterfaceColleague.selectShaderNodeName(self._channel)

            self._arnoldColleague.getUDimValues(self._channel)
            self._userInterfaceColleague.selectUDimValue(self._channel)

            self._userInterfaceColleague.getSceneLoopData(self._channel)
            self._arnoldColleague.render(self._channel)

    ## Event Handler ##
    def start(self):
        self._sceneLoad()
        self._loop()


# Abstract base class for the user interface colleague of the mediator pattern
class AbstractUserInterfaceColleague(object):
    def __init__(self, args):
        super(AbstractUserInterfaceColleague, self).__init__()
        self._args = args

    def getSceneLoadData(self, channel):
        raise NotImplementedError()

    def selectPolymeshNodeName(self, channel):
        raise NotImplementedError()

    def selectShaderNodeName(self, channel):
        raise NotImplementedError()

    def selectUDimValue(self, channel):
        raise NotImplementedError()

    def getSceneLoopData(self, channel):
        raise NotImplementedError()

    @staticmethod
    def addArguments(parser):
        raise NotImplementedError

#############
# Batch CLI #
#############

class BatchCommandLineColleague(AbstractUserInterfaceColleague):
    def __init__(self, args):
        super(BatchCommandLineColleague, self).__init__(args)
        if self._args.polymesh_node_name is None:
            raise InvalidInputError("no polymesh node name given")
        if self._args.shader_node_name is None:
            raise InvalidInputError()
        self._udimIndex = 0

    def getSceneLoadData(self, channel):
        if not isReadableFile(self._args.ass_file_path):
            raise InvalidInputError("%s is not a readable file" %
                                    self._args.ass_file_path)
        channel["ass_file_path"] = self._args.ass_file_path

    def selectPolymeshNodeName(self, channel):
        channel["polymesh_node_name"] = self._args.polymesh_node_name

    def selectShaderNodeName(self, channel):
        channel["shader_node_name"] = self._args.shader_node_name

    def selectUDimValue(self, channel):
        if self._args.enable_udim:
            channel["udim_value"] = channel["udim_values"][self._udimIndex]
            self._udimIndex += 1
            if self._udimIndex == len(channel["udim_values"]):
                channel["continue_loop"] = False # stop loop w/ sentinel
        else:
            channel["udim_value"] = None
            channel["continue_loop"] = False # stop loop w/ sentinel

    def getSceneLoopData(self, channel):
        channel["near_clip"] = self._args.near_clip
        channel["offset"] = self._args.offset
        channel["u_offset"] = self._args.u_offset
        channel["v_offset"] = self._args.v_offset
        channel["uv_set"] = self._args.uv_set
        channel["u_scale"] = self._args.u_scale
        channel["v_scale"] = self._args.v_scale

    @staticmethod
    def addArguments(parser):
        addOptoinalCommonArgs(parser)
        addRequiredAssFileArg(parser)
        addRequiredPolymeshAndShaderNodeNamesArgs(parser)
        addOptionalEnableUdimArg(parser)

###################
# Interactive CLI #
###################

def _inputFromStringList(inputName, strList):
    strList.sort()
    numOfStr = len(strList)
    choices = range(1, numOfStr+1)
    for i in choices:
        print "%d. %s" % (i, strList[i-1])
    rawInput = raw_input("Enter 1-%d for the %s (q to quit): " %
                         (numOfStr, inputName))
    if rawInput == "q": raise UserRequestedExit
    try:
        sel = int(rawInput)
    except ValueError:
        raise InvalidInputError('The value "%s" is not a valid int value for '
                                'a %s.' % (rawInput, inputName))
    if sel not in choices:
        raise InvalidInputError('The value "%s" is not a valid choice '
                                'from %s.' % (sel, choices))
    print 'You entered a choice of "%d" for %s.' % (sel, strList[sel-1])
    return strList[sel-1]

def _inputForType(inputName, inputType):
    rawInput = raw_input("Enter an %s value for %s (q to quit): " %
                         (inputType.__name__, inputName))
    if rawInput == "q": raise UserRequestedExit
    try:
        return inputType(rawInput)
    except ValueError:
        raise InvalidInputError('Given "%s" is not a valid %s type for %s.' %
                                (rawInput, inputType.__name__, inputName))

def _confirmDefault(default, inputName):
    rawInput = raw_input('Use %s "%s" ? [y/n/q]: ' % (inputName, default))
    if rawInput == "q": raise UserRequestedExit
    return rawInput == "y"

def _confirmOrInputForType(default, inputName, inputType):
    if _confirmDefault(default, inputName): return default
    return _inputForType(inputName, inputType)

class InteractiveCommandLineColleague(AbstractUserInterfaceColleague):

    def __init__(self, args):
        super(InteractiveCommandLineColleague, self).__init__(args)

    @staticmethod
    def _input(channel, default, name, inputType):
        channel[name] = _confirmOrInputForType(default,
                                                name.replace("_", " "),
                                                inputType)
    @staticmethod
    def _inputFromList(channel, name, listName):
        channel[name] = _inputFromStringList(name.replace("_", " "),
                                              channel[listName])

    def getSceneLoadData(self, channel):
        self._input(channel, self._args.ass_file_path, "ass_file_path",
                    types.StringType)
        if not isReadableFile(channel["ass_file_path"]):
            raise InvalidInputError("%s is not a readable file" %
                                    channel["ass_file_path"])

    def selectPolymeshNodeName(self, channel):
        self._inputFromList(channel, "polymesh_node_name",
                            "polymesh_node_names")

    def selectShaderNodeName(self, channel):
        self._inputFromList(channel, "shader_node_name", "shader_node_names")

    def selectUDimValue(self, channel):
        channel["udim_value"] = None
        rawInput = raw_input('Set udim value ? [y/n/q]: ')
        if rawInput == "q": raise UserRequestedExit
        if rawInput == "y":
            self._inputFromList(channel, "udim_value", "udim_values")

    def getSceneLoopData(self, channel):
        self._input(channel, self._args.near_clip, "near_clip",
                    types.FloatType)
        self._input(channel, self._args.offset, "offset", types.FloatType)
        self._input(channel, self._args.u_offset, "u_offset", types.FloatType)
        self._input(channel, self._args.v_offset, "v_offset", types.FloatType)
        self._input(channel, self._args.uv_set, "uv_set", types.StringType)
        self._input(channel, self._args.u_scale, "u_scale", types.FloatType)
        self._input(channel, self._args.v_scale, "v_scale", types.FloatType)

    @staticmethod
    def addArguments(parser):
        addOptoinalCommonArgs(parser)
        addRequiredAssFileArg(parser)


# TODO: Develop a PyQt interface
class PyQtColleague(AbstractUserInterfaceColleague):
    def __init__(self, args):
        super(PyQtColleague, self).__init__(args)

    def getSceneLoadData(self, channel):
        raise NotImplementedError()

    def selectPolymeshNodeName(self, channel):
        raise NotImplementedError()

    def selectShaderNodeName(self, channel):
        raise NotImplementedError()

    def selectUDimValue(self, channel):
        raise NotImplementedError()

    @staticmethod
    def addArguments(parser):
        raise NotImplementedError()


##########
# Arnold #
##########

class ArnoldColleague(object):
    def __init__(self, facade):
        super(ArnoldColleague, self).__init__()
        self._facade = facade

    def applySceneLoadData(self, channel):
        self._facade.sceneLoadAndSetup(channel["ass_file_path"])

    def getPolymeshNodeNames(self, channel):
        channel["polymesh_node_names"] = \
            self._facade.getPolymeshNodeNames()

    def getShaderNodeNames(self, channel):
        channel["shader_node_names"] = self._facade.getShaderNodeNames(
            channel["polymesh_node_name"])

    def getUDimValues(self, channel):
        channel["udim_values"] = self._facade.getUDimValues(
            channel["polymesh_node_name"])

    def render(self, channel):
        self._facade.render(
            channel["polymesh_node_name"], channel["shader_node_name"],
            channel["udim_value"], channel["near_clip"], channel["offset"],
            channel["u_offset" ], channel["v_offset"], channel["uv_set"],
            channel["u_scale"], channel["v_scale"]
        )

