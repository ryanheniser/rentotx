import inspect
import functools as ft

import arnold as ar

def _msgFuncWithModuleNameHeader(func, msgFormat, *params):
    stackFrameOfCaller = inspect.stack()[1] # caller's stack frame
    moduleOfCaller = inspect.getmodule(stackFrameOfCaller[0])
    headerText = moduleOfCaller.__name__ # Set the header text to module name
    if headerText is None: headerText = "__main__"
    func("[%s] %s" % (headerText, msgFormat), *params)

info = ft.partial(_msgFuncWithModuleNameHeader, ar.AiMsgInfo)
warn = ft.partial(_msgFuncWithModuleNameHeader, ar.AiMsgWarning)
error = ft.partial(_msgFuncWithModuleNameHeader, ar.AiMsgError)
