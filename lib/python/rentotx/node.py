# Arnold node related utility data/functions to help perform render to texture

import ctypes

import arnold as ar

from .exception import InviablePolymeshNode

def makeUniqueName(name):
    node = ar.AiNodeLookUpByName(name)
    if node is None: return name
    for i in range(10000):
        uname = name + str(i)
        node = ar.AiNodeLookUpByName(uname)
        if node is None: return uname
    raise RuntimeError("Excessive tries at making an unique node name "
                        "detected.")

def getAllNodes(mask):
    nodes = []
    nodeIter = ar.AiUniverseGetNodeIterator(mask)
    while not ar.AiNodeIteratorFinished(nodeIter):
        nodes.append(ar.AiNodeIteratorGetNext(nodeIter))
    ar.AiNodeIteratorDestroy(nodeIter)
    return nodes

def nodeSetArrayString(node, name, value):
    value_size = len(value)
    # noinspection PyCallingNonCallable
    array = ar.AiArrayConvert(value_size, 1, ar.AI_TYPE_STRING,
                                  (ctypes.c_char_p * value_size)(*value))
    ar.AiNodeSetArray(node, name, array)

# Raises an InviablePolymeshNode exception when a given polymesh node is
# inviable. A viable polymesh node in this context is one that has all the
# properties necessary to perform a render to texture request on it.
def confirmViablePolymeshNode(node):
    #  Polymesh nodes with no shaders are inviable
    rootShaderNode = ar.cast(ar.AiNodeGetPtr(node, "shader"),
                             ar.POINTER(ar.AtNode))
    if rootShaderNode is None:
        raise InviablePolymeshNode("no value for shader (POINTER) parameter")

    #  Polymesh nodes that are not a polygon mesh are inviable (e.g. subd)
    if ar.AiNodeGetInt(node, "subdiv_type") != 0:
        nodeEntry = ar.AiNodeGetNodeEntry(node)
        paramEntry = ar.AiNodeEntryLookUpParameter(nodeEntry, "subdiv_type")
        subdivType = ar.AiEnumGetString(ar.AiParamGetEnum(paramEntry),
                                        ar.AiNodeGetInt(node, "subdiv_type"))
        raise InviablePolymeshNode("not a polygon mesh (subdiv_type has a "
                                   "value of %s)" % subdivType)

    # Polymesh nodes with no UVs are inviable
    uvListArray = ar.AiNodeGetArray(node, "uvlist")
    if uvListArray is None or ar.AiArrayGetNumElements(uvListArray.contents) < 1:
        raise InviablePolymeshNode("no uvlist values")

    # Polymesh nodes that are invisible are inviable
    if ar.AiNodeGetByte(node, "visibility") == 0:
        raise InviablePolymeshNode("invisible to the camera")
