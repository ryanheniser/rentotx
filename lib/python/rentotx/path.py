import os
import os.path

# Normalized absolute real path - symlinks can contain relative paths, hence
# the need to use both
def normAbsRealPath(path):
    return os.path.abspath(os.path.realpath(path))

def isReadableFile(path):
    return os.path.isfile(path) and os.access(path, os.R_OK)

