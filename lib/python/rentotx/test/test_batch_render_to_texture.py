import os
import os.path
import argparse
import tempfile
import subprocess
import unittest

import arnold as ar

import rentotx as rt

class TestBatchRenderToTexture(unittest.TestCase):

    _diffFailThreshold = 0.2
    _diffFailePercent = 0.1

    def setUp(self):
        # Get the path of directory that contains this file
        self._dirPathOfThisFile = os.path.dirname(os.path.realpath(__file__))
        if not os.path.isdir(self._dirPathOfThisFile):
            self.fail("%s is not a directory" % self._dirPathOfThisFile)

        self._outputDirectoryPath = os.path.join(self._dirPathOfThisFile,
                                                 "out")
        self._sceneDirectoryPath = os.path.join(self._dirPathOfThisFile,
                                                "scene")
        self._textureDirectoryPath = os.path.join(self._sceneDirectoryPath,
                                                  "tx")

        self._previousWorkingDir = os.getcwd()
        os.chdir(self._dirPathOfThisFile)

    def _startBatchRenderToTexture(self):
        arnoldSourceSceneFilePath = os.path.join(
            self._sceneDirectoryPath, self._arnoldSourceSceneFilePath)
        if not os.path.isfile(arnoldSourceSceneFilePath):
            self.fail("%s is not a file" % arnoldSourceSceneFilePath)

        # Parse mock arguments for the batch command line interface
        mockArgs = [
            arnoldSourceSceneFilePath,  # ass_file_path
            "test_sphere",              # polymesh_node_name
            self._shaderNodeName,       # shader_node_name
        ]
        if self._enableUdim:
            mockArgs.append("--enable_udim")
        parser = argparse.ArgumentParser()
        rt.BatchCommandLineColleague.addArguments(parser)
        args = parser.parse_args(args=mockArgs)

        # Get a temp log file path
        logFilePath = ""
        with tempfile.NamedTemporaryFile() as tmpfile:
            logFilePath = tmpfile.name

        # Create a context manager for a mediator to execute in by creating a
        # render to texture facade
        with rt.RenderToTextureFacade(logFilePath) as facade:
            mediator = rt.UserInterfaceAndArnoldMediator(
                rt.BatchCommandLineColleague(args),
                rt.ArnoldColleague(facade)
            )
            mediator.start()

        # Test the return code
        try:
            self.assertEqual(facade.returnCode, ar.AI_SUCCESS)
        except AssertionError:
            with open(logFilePath, 'r') as myfile:
                print "\n"
                print myfile.read()
            raise

        os.remove(logFilePath)

    def _runOpenImageIOToolDiff(self):
        # Test render output with diff (use some threshold and percentage to
        # account for LSB error)
        referenceFilePath = os.path.join(self._dirPathOfThisFile, "ref",
                                         self._outputFileName)
        outputFilePath = os.path.join(self._outputDirectoryPath,
                                      self._outputFileName)
        cmd = "oiiotool  --fail %f --failpercent %f --diff %s %s" % \
              (self._diffFailThreshold, self._diffFailePercent,
               referenceFilePath,  outputFilePath)
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)
        output = p.communicate()
        self.assertEqual(p.returncode, 0, msg=output)

    def test_surface_shader(self):
        self._arnoldSourceSceneFilePath = "surface_shader.ass"
        self._shaderNodeName = "test_lambert"
        self._enableUdim = False
        self._startBatchRenderToTexture()
        self._outputFileName = "surface_shader.exr"
        self._runOpenImageIOToolDiff()

    def test_procedural_shader(self):
        self._arnoldSourceSceneFilePath = "procedural_shader.ass"
        self._shaderNodeName = "test_noise"
        self._enableUdim = False
        self._startBatchRenderToTexture()
        self._outputFileName = "procedural_shader.exr"
        self._runOpenImageIOToolDiff()

    def test_image_shader(self):
        self._arnoldSourceSceneFilePath = "image_shader.ass"
        self._shaderNodeName = "test_image"
        self._enableUdim = False
        self._startBatchRenderToTexture()
        self._outputFileName = "image_shader.exr"
        self._runOpenImageIOToolDiff()

    def test_image_shader_with_bump(self):
        self._arnoldSourceSceneFilePath = "image_shader_with_bump.ass"
        self._shaderNodeName = "test_image"
        self._enableUdim = False
        self._startBatchRenderToTexture()
        self._outputFileName = "image_shader_with_bump.exr"
        self._runOpenImageIOToolDiff()

    def test_image_shader_with_displacement(self):
        self._arnoldSourceSceneFilePath = "image_shader_with_displacement.ass"
        self._shaderNodeName = "test_image"
        self._enableUdim = False
        self._startBatchRenderToTexture()
        self._outputFileName = "image_shader_with_displacement.exr"
        self._runOpenImageIOToolDiff()

    def test_image_shader_with_udims(self):
        self._arnoldSourceSceneFilePath = "image_shader_with_udim.ass"
        self._shaderNodeName = "test_image"
        self._enableUdim = True
        self._startBatchRenderToTexture()
        self._outputFileName = "image_shader_with_udim.1001.exr"
        self._runOpenImageIOToolDiff()
        self._outputFileName = "image_shader_with_udim.1002.exr"
        self._runOpenImageIOToolDiff()
        self._outputFileName = "image_shader_with_udim.1011.exr"
        self._runOpenImageIOToolDiff()
        self._outputFileName = "image_shader_with_udim.1012.exr"
        self._runOpenImageIOToolDiff()

    def test_procedural_shader_with_udims(self):
        self._arnoldSourceSceneFilePath = "procedural_shader_with_udim.ass"
        self._shaderNodeName = "test_noise"
        self._enableUdim = True
        self._startBatchRenderToTexture()
        self._outputFileName = "image_shader_with_udim.1001.exr"
        self._runOpenImageIOToolDiff()
        self._outputFileName = "image_shader_with_udim.1002.exr"
        self._runOpenImageIOToolDiff()
        self._outputFileName = "image_shader_with_udim.1011.exr"
        self._runOpenImageIOToolDiff()
        self._outputFileName = "image_shader_with_udim.1012.exr"
        self._runOpenImageIOToolDiff()

    def tearDown(self):
        os.chdir(self._previousWorkingDir)
