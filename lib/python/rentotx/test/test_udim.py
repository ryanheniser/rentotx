import unittest
import arnold as ar

import rentotx as rt

class TestValidValues(unittest.TestCase):

    def test_valid_u_and_v(self):
        tile = rt.MariUDimlTile(u=0.1345, v=0.548)
        self.assertEqual(tile.intU, 0)
        self.assertEqual(tile.intV, 0)
        self.assertEqual(tile.uDim, 1001)
        tile = rt.MariUDimlTile(u=3.435, v=2.535)
        self.assertEqual(tile.intU, 3)
        self.assertEqual(tile.intV, 2)
        self.assertEqual(tile.uDim, 1024)

class TestBoundaryValues(unittest.TestCase):

    def test_u_zero_v_zero(self):
        tile = rt.MariUDimlTile(u=0.0, v=0.0)
        self.assertEqual(tile.intU, 0)
        self.assertEqual(tile.intV, 0)
        self.assertEqual(tile.uDim, 1001)

    def test_u_epsilon_v_epsilon(self):
        tile = rt.MariUDimlTile(u=ar.AI_EPSILON, v=ar.AI_EPSILON)
        self.assertEqual(tile.intU, 0)
        self.assertEqual(tile.intV, 0)
        self.assertEqual(tile.uDim, 1001)

    def test_u_one_v_zero(self):
        tile = rt.MariUDimlTile(u=1.0, v=0.0)
        self.assertEqual(tile.intU, 0)
        self.assertEqual(tile.intV, 0)
        self.assertEqual(tile.uDim, 1001)

    def test_u_zero_v_one(self):
        tile = rt.MariUDimlTile(u=0.0, v=1.0)
        self.assertEqual(tile.intU, 0)
        self.assertEqual(tile.intV, 0)
        self.assertEqual(tile.uDim, 1001)

    def test_u_one_plus_epsilon_v_zero(self):
        tile = rt.MariUDimlTile(u=1.0+ar.AI_EPSILON, v=0.0)
        self.assertEqual(tile.intU, 1)
        self.assertEqual(tile.intV, 0)
        self.assertEqual(tile.uDim, 1002)

    def test_u_zero_v_one_plus_epsilon(self):
        tile = rt.MariUDimlTile(u=0.0, v=1.0+ar.AI_EPSILON)
        self.assertEqual(tile.intU, 0)
        self.assertEqual(tile.intV, 1)
        self.assertEqual(tile.uDim, 1011)

    def test_u_two_plus_epsilon_v_zero(self):
        tile = rt.MariUDimlTile(u=2.0+ar.AI_EPSILON, v=0.0)
        self.assertEqual(tile.intU, 2)
        self.assertEqual(tile.intV, 0)
        self.assertEqual(tile.uDim, 1003)

    def test_u_zero_v_two_plus_epsilon(self):
        tile = rt.MariUDimlTile(u=0.0, v=2.0+ar.AI_EPSILON)
        self.assertEqual(tile.intU, 0)
        self.assertEqual(tile.intV, 2)
        self.assertEqual(tile.uDim, 1021)

class TestInvalidValues(unittest.TestCase):

    def test_u_negative_v_positive(self):
        with self.assertRaises(ValueError):
            rt.MariUDimlTile(u=-2.5, v=1.532)

    def test_u_positive_v_negative(self):
        with self.assertRaises(ValueError):
            rt.MariUDimlTile(u=3.353, v=-5.732)

    def test_u_greater_than_ulimit(self):
        with self.assertRaises(ValueError):
            rt.MariUDimlTile(u=22.0, v=1.532)

    def test_v_greater_than_ulimit(self):
        with self.assertRaises(ValueError):
            rt.MariUDimlTile(u=3.436, v=12.0)

if __name__ == '__main__':
    unittest.main()