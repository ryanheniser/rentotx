import ctypes
import pprint

import arnold as ar

# ShaderTree is a simple tree data structure with tree nodes made up of a
# 2-tuple with the first element being the shader node name and the second
# element being a dictionary with parameter name (includeing components) keys
# and node values.
class ShaderTree(object):
    def __init__(self, shapeNode):
        super(ShaderTree, self).__init__()
        rootShaderNode = ar.cast(ar.AiNodeGetPtr(shapeNode, "shader"),
                                     ar.POINTER(ar.AtNode))
        self._rootShaderNodeName = ar.AiNodeGetName(rootShaderNode)
        self._shaderTree = (self._rootShaderNodeName, {})

        self._shaderNodeNamesToNodes = {} # For efficient shader node access
        self._shaderNodeNamesToSubtree = {} # For efficient subtree access
        self._shaderNodeNamesToParentNodeAndParamName = {} # For efficient
                                                           # link finding

        self._getAllLinkedNodesForAllParameters(rootShaderNode,
                                                self._shaderTree[1])

        self._shaderNodeNamesToSubtree[self._rootShaderNodeName] = \
            (self._rootShaderNodeName, self._shaderTree[1])
        self._shaderNodeNamesToParentNodeAndParamName[
            self._rootShaderNodeName] = (shapeNode, "shader")

    def __str__(self):
        pp = pprint.PrettyPrinter(indent=1, width=10)
        return pp.pformat(self._shaderTree)

    def getRootShaderNodeName(self):
        return self._rootShaderNodeName

    def getShaderNodeNames(self):
        return self._shaderNodeNamesToSubtree.keys()

    def getShaderNode(self, shaderNodeName):
        return self._shaderNodeNamesToNodes[shaderNodeName]

    def getTree(self):
        return self._shaderNodeNamesToSubtree

    def getSubtree(self, shaderNodeName):
        return self._shaderNodeNamesToSubtree.get(shaderNodeName)

    def getParentNodeAndParameter(self, shaderNodeName):
        return self._shaderNodeNamesToParentNodeAndParamName.get(
            shaderNodeName)

    def _getAllLinkedNodesForParameter(self, node, paramName, paramType,
                                       subTree):
        comp = ctypes.c_int() # -1 for the whole output,
                              # [0..3] for a single component
        shaderNode = ar.AiNodeGetLink(node, paramName, comp)
        if shaderNode is None: return
        shaderNodeName = ar.AiNodeGetName(shaderNode)

        if comp.value == -1 or paramType == ar.AI_TYPE_FLOAT:
            # Whole output or float type
            subTree[paramName] = (shaderNodeName, {})
            self._getAllLinkedNodesForAllParameters(shaderNode,
                                                    subTree[paramName][1])
            self._shaderNodeNamesToNodes[shaderNodeName] = shaderNode
            self._shaderNodeNamesToSubtree[shaderNodeName] = subTree[paramName]
            self._shaderNodeNamesToParentNodeAndParamName[shaderNodeName] = \
                (node, paramName)
        else:
            # single component
            typeToComponents = {
                ar.AI_TYPE_RGB     : ("r", "g", "b"),
                ar.AI_TYPE_RGBA    : ("r", "g", "b", "a"),
                ar.AI_TYPE_VECTOR  : ("x", "y", "z"),
                ar.AI_TYPE_POINT   : ("x", "y", "z"),
                ar.AI_TYPE_POINT2  : ("x", "y"),
            }
            paramNameWithComp = paramName + '.' \
                                + typeToComponents.get(paramType)[comp.value]
            subTree[paramNameWithComp] = (shaderNodeName, {})
            self._getAllLinkedNodesForAllParameters(
                shaderNode, subTree[paramNameWithComp][1])
            self._shaderNodeNamesToNodes[shaderNodeName] = shaderNode
            self._shaderNodeNamesToSubtree[shaderNodeName] = \
                subTree[paramNameWithComp]
            self._shaderNodeNamesToParentNodeAndParamName[shaderNodeName] = \
                (node, paramNameWithComp)

    def _getAllLinkedNodesForAllParameters(self, node, subTree):
        paramIter = ar.AiNodeEntryGetParamIterator(ar.AiNodeGetNodeEntry(node))
        while not ar.AiParamIteratorFinished(paramIter):
            paramEntry = ar.AiParamIteratorGetNext(paramIter)
            paramType = ar.AiParamGetType(paramEntry)
            paramName = ar.AiParamGetName(paramEntry)

            if paramType == ar.AI_TYPE_ARRAY: # special case array parameters
                array = ar.AiNodeGetArray(node, paramName).contents
                for i in xrange(array.nelements):
                    self._getAllLinkedNodesForParameter(
                        node, "%s[%d]" % (paramName, i), array.type, subTree)
            else:
                self._getAllLinkedNodesForParameter(
                    node, paramName, paramType, subTree)
        ar.AiParamIteratorDestroy(paramIter)

