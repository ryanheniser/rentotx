import math

import arnold as ar

from .message import warn

# An abstract base class that represents how UDimensional values are computed
#
# Standard uv coordinates go from [0.0, 1.0]. Note, we have used square
# brackets for this interval, which means 0.0 and 1.0 are included.
#
# Standard UDim formula (set by Weta circa 2002):
#
# UDim(U,V) = base + offset + (U + 1) + (V * ulimit)
#
# where U is the integer portion of the u coordinate, and V is the integer
# portion of the v coordinate.
#
# The offset is sometimes used for splitting the textures up based on position,
# e.g. left and right; But, the offset is typically 0. The ulimit is typically
# 10. So, an uv coordinate value of (1.0, 1.0) would be
#
# UDim(U=1, V=1) = 1000+(1+1)+(1*10)=1012
#
# Note, the boundary value starts a new UDim tile. That is, the uv values for
# each tile goes from [0.0, 1.0), # [1.0, 2.0), [2.0, 3.0)... Arnold has some
# special handling for UDIMs and tiles where it will find out if a particular
# polygon is, in the majority, in a given tile, and the particular UV shading
# point is on the border or just over it, it will still be considered in the
# majority tile. It does a good job in that regard, so that Arnold doesn't
# accidentally request UDIMs that don't exist (and were never intended to
# exist). It is a good idea to suck in your uv values from the edges of the
# tile just a little bit to avoid these types of situations anyway.
class AbstractUDimlTile(object):
    _base = NotImplemented # Integer value that a derived class must set
    _offset = NotImplemented # Integer value that a derived class must set
    _uLimit = NotImplemented # Integer value that a derived class must set

    def __init__(self, u, v):
        super(AbstractUDimlTile, self).__init__()
        uLimitFloat = float(self._uLimit)
        if u > uLimitFloat or u < 0.0:
            raise ValueError("u value %f is out of UDim range %f >= u >= %f" %
                             (u, uLimitFloat, 0.0))
        if v > uLimitFloat or v < 0.0:
            raise ValueError("v value %f is out of UDim range %f >= v >= %f" %
                             (v, uLimitFloat, 0.0))

        uMinusEpsilonIntU = int(math.floor(max(u - ar.AI_EPSILON, 0.0)))
        self._intU = int(math.floor(u))
        # Special case the boundary values
        if self._intU > uMinusEpsilonIntU: self._intU = uMinusEpsilonIntU

        vMinusEpsilonIntV = int(math.floor(max(v - ar.AI_EPSILON, 0.0)))
        self._intV = int(math.floor(v))
        # Special case the boundary values
        if self._intV > vMinusEpsilonIntV: self._intV = vMinusEpsilonIntV

        self._uDim = self._computeUDim()

    def _computeUDim(self):
        return self._base \
               + self._offset \
               + (self._intU + 1) \
               + (self._intV * self._uLimit)

    @property
    def intU(self):
        return self._intU

    @property
    def intV(self):
        return self._intV

    @property
    def uDim(self):
        return self._uDim

    def __lt__(self, other):
        return self.uDim.__lt__(other.uDim)

    def __le__(self, other):
        return self.uDim.__le__(other.uDim)

    def __eq__(self, other):
        return self.uDim.__eq__(other.uDim)

    def __ne__(self, other):
        return self.uDim.__ne__(other.uDim)

    def __gt__(self, other):
        return self.uDim.__gt__(other.uDim)

    def __ge__(self, other):
        return self.uDim.__ge__(other.uDim)

    def __repr__(self):
        return self._uDim.__repr__()

    def __str__(self):
        return self._uDim.__str__()

class MariUDimlTile(AbstractUDimlTile):
    # Industry Standard UDim that is used in Mari (set by Weta)
    # UDIM = base + offset + (U + 1) + (V * ulimit)
    # UDIM = 1000 + 0 + (U + 1) + (V * 10)
    # UDIM (U=0, V=1) = 1000+(0+1)+(1*10)=1011
    _base = 1000
    _offset = 0
    _uLimit = 10

def getUDimValuesToUDimTile(polymeshNode, uDimTileClass):
    result = {}
    uvListArray = ar.AiNodeGetArray(polymeshNode, "uvlist")
    nElem = ar.AiArrayGetNumElements(uvListArray.contents)
    for uv in [ar.AiArrayGetVec2(uvListArray, i) for i in xrange(nElem)]:
        try:
            uDimTile = uDimTileClass(uv.x, uv.y)
        except ValueError, e:
            warn("Invalid UV coordinates (%f, %f) on node %s for UDim - "
                 "%s." % (uv.x, uv.y, ar.AiNodeGetName(polymeshNode), str(e)))
            continue
        result[uDimTile.uDim] = uDimTile
    return result

